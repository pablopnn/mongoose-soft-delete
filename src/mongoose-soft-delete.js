'use strict';

module.exports = function(schema) {

    schema.add({
        deleted: Date
    });

    schema.pre('save', function (next) {
        if (this.deleted == undefined) {
            this.deleted = null;
        }

        next();
    });

    var queries = [
        'find',
        'findOne',
        'findOneAndUpdate',
        'update',
        'count'
    ];

    queries.forEach(function(query) {

        var conditions = {
            deleted: {
                '$eq': null
            }
        };

        schema.pre(query, function(next) {
            if (!this._conditions['deleted']) {
                this.where(conditions);
            }
            next();
        });
    });

    schema.statics.remove = function(first, second) {
        var callback;
        var update = {
            deleted: new Date()
        };
        var conditions;
        var options = {
            multi: false
        };

        if (typeof first === 'function') {
            callback = first;
            conditions = {};
            options.multi = true;
        } else {
            callback = second;
            conditions = first;
        }

        if (typeof callback !== 'function') {
            throw ('Wrong arguments!');
        }

        this.update(conditions, update, options, function(err, numAffected) {
            if (err) {
                return callback(err);
            }
            callback(null, numAffected);
        });
    };

    schema.methods.remove = function(callback) {
        this.deleted = new Date();
        this.save(callback);
    };
};