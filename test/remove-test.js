var fixtures = require('./test-fixtures');
var Test = require('./test-model');
var should = require('should');

describe("remove(); Statics: ", function() {

	it("Should `remove` all documents.", function(done) {

		Test.remove(function(err) {
			should.not.exist(err);
			Test.find(function(err, tests) {

				should.not.exist(err);
				tests.should.be.instanceof(Array);
				tests.should.be.instanceof(Array).and.have.lengthOf(0);

				done();
			});
		});
	});

	it("Should set removed to true.", function(done) {

		Test.remove({
			name: 'test2'
		}, function(err, doc) {
			should.not.exist(err);

			Test.collection.findOne({
				name: 'test2'
			}, function(err, doc) {

                doc.deleted.should.be.a.Date;
				done();
			});
		});
	});
});

describe("remove(); Methods: ", function() {

	it("Should set removed document to true.", function(done) {
		Test.findOne({
			name: 'default'
		}, function(err, doc) {

			should.not.exist(err);
			should.not.exist(doc.deleted);

			doc.remove(function(err) {

				should.not.exist(err);
				Test.collection.findOne({
					name: 'default'
				}, function(err, doc) {

					doc.deleted.should.be.a.Date;
					done();
				});
			});
		});
	});
});